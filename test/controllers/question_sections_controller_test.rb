require 'test_helper'

class QuestionSectionsControllerTest < ActionController::TestCase
  setup do
    @question_section = question_sections(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:question_sections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create question_section" do
    assert_difference('QuestionSection.count') do
      post :create, question_section: { question_id: @question_section.question_id, section_id: @question_section.section_id, weight: @question_section.weight }
    end

    assert_redirected_to question_section_path(assigns(:question_section))
  end

  test "should show question_section" do
    get :show, id: @question_section
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @question_section
    assert_response :success
  end

  test "should update question_section" do
    patch :update, id: @question_section, question_section: { question_id: @question_section.question_id, section_id: @question_section.section_id, weight: @question_section.weight }
    assert_redirected_to question_section_path(assigns(:question_section))
  end

  test "should destroy question_section" do
    assert_difference('QuestionSection.count', -1) do
      delete :destroy, id: @question_section
    end

    assert_redirected_to question_sections_path
  end
end
