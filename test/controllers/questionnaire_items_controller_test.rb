require 'test_helper'

class QuestionnaireItemsControllerTest < ActionController::TestCase
  setup do
    @questionnaire_item = questionnaire_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:questionnaire_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create questionnaire_item" do
    assert_difference('QuestionnaireItem.count') do
      post :create, questionnaire_item: { break: @questionnaire_item.break, content_id: @questionnaire_item.content_id, order: @questionnaire_item.order, question_id: @questionnaire_item.question_id }
    end

    assert_redirected_to questionnaire_item_path(assigns(:questionnaire_item))
  end

  test "should show questionnaire_item" do
    get :show, id: @questionnaire_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @questionnaire_item
    assert_response :success
  end

  test "should update questionnaire_item" do
    patch :update, id: @questionnaire_item, questionnaire_item: { break: @questionnaire_item.break, content_id: @questionnaire_item.content_id, order: @questionnaire_item.order, question_id: @questionnaire_item.question_id }
    assert_redirected_to questionnaire_item_path(assigns(:questionnaire_item))
  end

  test "should destroy questionnaire_item" do
    assert_difference('QuestionnaireItem.count', -1) do
      delete :destroy, id: @questionnaire_item
    end

    assert_redirected_to questionnaire_items_path
  end
end
