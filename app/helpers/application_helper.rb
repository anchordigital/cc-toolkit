module ApplicationHelper
	def active_class(link_path)
	  current_page?(link_path) ? "active" : ""
	 end

	# def active_parent_class(link_path)
	# 	current_page?(controller: link_path)
	# end

	def parent_nav_is_active?(path)
	    rp=request.path
	    if rp == path
	      "parent-active"
	    elsif rp.include? path #child page check
	      "parent-active"
	    end
	  end
end
