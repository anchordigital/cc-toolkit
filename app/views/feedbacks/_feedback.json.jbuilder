json.extract! feedback, :id, :name, :text, :low, :high, :section_id, :created_at, :updated_at
json.url feedback_url(feedback, format: :json)
