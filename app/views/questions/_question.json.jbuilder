json.extract! question, :id, :question, :section_id, :created_at, :updated_at
json.url question_url(question, format: :json)
