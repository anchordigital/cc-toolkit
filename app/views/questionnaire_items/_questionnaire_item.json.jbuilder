json.extract! questionnaire_item, :id, :question_id, :content_id, :break, :order, :created_at, :updated_at
json.url questionnaire_item_url(questionnaire_item, format: :json)
