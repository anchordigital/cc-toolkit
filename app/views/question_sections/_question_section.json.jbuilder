json.extract! question_section, :id, :question_id, :section_id, :weight, :created_at, :updated_at
json.url question_section_url(question_section, format: :json)
