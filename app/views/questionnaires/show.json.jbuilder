# json.partial! "questionnaires/questionnaire", questionnaire: @questionnaire
json.questionaires do 
  # json.name @questionaire.name
  json.array! @questionnaire_items do |questionnaire_item|
    json.name questionnaire_item.question
    json.name questionnaire_item.heading
  end
end

# json.array! @questionnaire_items do |question|
#     json.name question.id
#   end