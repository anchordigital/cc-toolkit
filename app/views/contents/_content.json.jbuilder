json.extract! content, :id, :heading, :text, :created_at, :updated_at
json.url content_url(content, format: :json)
