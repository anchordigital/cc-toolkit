json.extract! question_input, :id, :question_id, :order, :input, :created_at, :updated_at
json.url question_input_url(question_input, format: :json)
