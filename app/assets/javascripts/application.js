// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui.min
//= require foundation
//= require turbolinks
//= require_tree .
//= require html.sortable
//= require cocoon

$(function(){ $(document).foundation(); });

// QUESTION JS FOR DRAGGABLE ITEMS
// SET UP SORTABLE
// SAVE SORTABLE POS
var ready, set_positions;

set_positions = function(){
    // loop through and give each task a data-pos
    // attribute that holds its position in the DOM
    $('.question').each(function(i){
        $(this).attr("data-pos",i+1);
        if($(this).find('.row--item__order').length){
            $(this).find('.row--item__order').html(i+1);
        }
    });
}

ready = function(){

    // call set_positions function
    set_positions();

    $('.sortable').sortable();

    // after the order changes
    $('.sortable').sortable().bind('sortupdate', function(e, ui) {
        // array to store new order
        updated_order = []
        // set the updated positions
        set_positions();

        // populate the updated_order array with the new task positions
        $('.row--item').each(function(i){
        	console.log($(this).data("id"), i+1);
            updated_order.push({ id: $(this).data("id"), order: i+1 });
            console.log(updated_order);
        });

        // send the updated order via ajax
        $.ajax({
            type: "PUT",
            url: '/questionnaire_items/sort',
            data: { order: updated_order },
            success: function (data, status, error) {
                console.log('success', data);
                
            },
            error: function (data, status, error) {
              console.log('error', data, status, error);
            }
        });
    });

    $( ".authorise-user" ).on( "click", function() {
        
        // send the updated order via ajax  
        $.ajax({
            type: "PUT",
            url: '/users/authorise',
            data: { id: $(this).data("id") }
        });

    });

    $( ".create-break" ).on( "click", function() {
        console.log("line 81");
        // send the updated order via ajax  
        $.ajax({
            type: "PUT",
            url: '/questionnaire_items/createbreak',
            data: { id: $(this).data("id") },
            success: function (data, status, error) {
                console.log('success', data);
                setTimeout(function(){ $('.sortable').sortable(); }, 200);
            },
            error: function (data, status, error) {
              console.log('error', data, status, error);
            }
        });

    });

}

$(document).ready(ready);
/**
 * if using turbolinks
 */
$(document).on('page:load', ready);