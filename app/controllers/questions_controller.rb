class QuestionsController < ApplicationController

  before_filter :authorize
  
  before_action :set_question, only: [:show, :edit, :update, :destroy]

  # GET /questions
  # GET /questions.json
  def index
    @questions = Question.all
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
  end

  # GET /questions/new
  def new
    @question = Question.new
  end

  # GET /questions/1/edit
  def edit
  end

  # GET /questions/1/edit
  def section
    @question = @section.questions
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params)

    # Get new order value
    @order = QuestionnaireItem.maximum("order") + 1

    respond_to do |format|
      if @question.save
        format.html { redirect_to questionnaire_items_path, notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @question }

        @questionnaire_item =  QuestionnaireItem.new(:question_id => @question.id, :content_id => '', :break => '', :order => @order, :questionnaire_id => 1)
        @questionnaire_item.save

      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      if @question.update(question_params)
        format.html { redirect_to @question, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to questionnaire_items_path, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # def sort
  #   params[:order].each do |key,value|
  #     Question.find(value[:id]).update_attribute(:order,value[:order])
  #   end
  #   render :nothing => true
  # end

  def sort
    params[:order].each do |key,value|
        questions = Question.includes(:question_sections).find(value[:id])
        questions.question_sections.each { |q| q.update_attribute(:order,value[:order]) }
    end
    render :nothing => true
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:question, :low_label, :high_label,
        question_sections_attributes: [:id, :question_id, :section_id, :weight, :order, :_destroy ],
        question_inputs_attributes: [:id, :question_id, :order, :input, :_destroy ])
    end
end
