class QuestionnaireItemsController < ApplicationController
  before_action :set_questionnaire_item, only: [:show, :edit, :update, :destroy]

  # GET /questionnaire_items
  # GET /questionnaire_items.json
  def index
    # @questionnaire_items = QuestionnaireItem.all
    # @questions = Question.all
    # @content = Content.all

    @questionnaire_items = QuestionnaireItem.includes(:question, :content).all.order(:order)
    respond_to do |format|
       format.json
       format.html
     end
    # @questionnaire_items.each do |QuestionnaireItem|
    #      QuestionnaireItem.question
    # end
  end

  # GET /questionnaire_items/1
  # GET /questionnaire_items/1.json
  def show
  end

  # GET /questionnaire_items/new
  def new
    @questionnaire_item = QuestionnaireItem.new
  end

  # GET /questionnaire_items/1/edit
  def edit
  end

  # POST /questionnaire_items
  # POST /questionnaire_items.json
  def create
    @questionnaire_item = QuestionnaireItem.new(questionnaire_item_params)

    respond_to do |format|
      if @questionnaire_item.save
        format.html { redirect_to @questionnaire_item, notice: 'Questionnaire item was successfully created.' }
        format.json { render :show, status: :created, location: @questionnaire_item }
      else
        format.html { render :new }
        format.json { render json: @questionnaire_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def createbreak
    # Get new order value
    @order = QuestionnaireItem.maximum("order") + 1

    # Create break as question item
    @questionnaire_item =  QuestionnaireItem.new(:question_id => '', :content_id => '', :break => 1, :order => @order, :questionnaire_id => 1)
    @questionnaire_item.save

    render :nothing => true
  end

  # PATCH/PUT /questionnaire_items/1
  # PATCH/PUT /questionnaire_items/1.json
  def update
    respond_to do |format|
      if @questionnaire_item.update(questionnaire_item_params)
        format.html { redirect_to @questionnaire_item, notice: 'Questionnaire item was successfully updated.' }
        format.json { render :show, status: :ok, location: @questionnaire_item }
      else
        format.html { render :edit }
        format.json { render json: @questionnaire_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questionnaire_items/1
  # DELETE /questionnaire_items/1.json
  def destroy
    @questionnaire_item.destroy
    respond_to do |format|
      format.html { redirect_to questionnaire_items_url, notice: 'Questionnaire item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def sort
    params[:order].each do |key,value|
        # questions = Question.includes(:question_sections).find(value[:id])
        # questions.question_sections.each { |q| q.update_attribute(:order,value[:order]) }

        QuestionnaireItem.find(value[:id]).update_attribute(:order,value[:order])
    end
    render :nothing => true
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_questionnaire_item
      @questionnaire_item = QuestionnaireItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def questionnaire_item_params
      params.require(:questionnaire_item).permit(:question_id, :content_id, :break, :order)
    end
end
