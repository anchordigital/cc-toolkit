class Api::QuestionnairesController < ApplicationController
	def index
		@questionnaires = Questionnaire.all
		respond_to do |format|
		  #format.json  { render :json => @questionnaires.to_json(:include => [questions: {include: :question_inputs}], ), :callback => params['callback']}
		  format.json  { render :json => @questionnaires.to_json(:include => [questionnaire_items: {include: :question,},], ), :callback => params['callback']}
		end
	end

	def show
		@questionnaires = Questionnaire.find(params[:id])
		# @questions = @section.questions
		respond_to do |format|
		  #format.json  { render :json => @questionnaires.to_json(:include => [:questions]), :callback => params['callback']}
		  format.json  { render :json => @questionnaires.to_json(:include => [ questionnaire_items: {include: :question}, questionnaire_items: {include: :content} ], ), :callback => params['callback']}
		end
	end

	def full
		render json: Questionnaires.all
	end

end
