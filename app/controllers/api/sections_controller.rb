class Api::SectionsController < ApplicationController
	def index
		@section = Section.all
		respond_to do |format|
		  format.json  { render :json => @section.to_json(:include => [questions: {include: :question_inputs}], ), :callback => params['callback']}
		end

	end

	def show
		@section = Section.find(params[:id])
		@questions = @section.questions
		respond_to do |format|
		  format.json  { render :json => @section.to_json(:include => [:questions]), :callback => params['callback']}
		end
	end

	def full
		render json: Section.all
	end

end