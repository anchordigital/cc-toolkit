class Api::QuestionsController < ApplicationController
	def index
		#render json: Question.all
		@question = Question.all
		respond_to do |format|
		  format.json  { render :json => @question.to_json(:include => [:question_inputs, :sections]), :callback => params['callback']}
		end
	end

	def show
		list = Question.find(params[:id])
		render json: list
	end
end