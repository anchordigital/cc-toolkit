class Api::FeedbacksController < ApplicationController
	def index
		render json: Feedback.all
	end

	def show
		@feedbacks = Feedback.find(params[:id])
		render json: @feedbacks
	end

	def score
		@feedback = Feedback.where("low < ?", params[:score]).where("high > ?", params[:score]).where(:section_id => params[:section_id])
		#@feedback = Feedback.find(params[:score])
		
		#render json: @feedback
		 
		respond_to do |format|
		  format.json  { render :json => @feedback.to_json(), :callback => params['callback']}
		end
	end

end