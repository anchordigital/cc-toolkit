class Api::QuestionnaireItemsController < ApplicationController
	def index
		@questionnaire_items = QuestionnaireItem.includes(:question, :content).all.order(:order)
		respond_to do |format|
		  format.json  { render :json => @questionnaire_items.to_json( :include => [ :content, question: {include: [:question_inputs, :sections]} ] ), :callback => params['callback']}
		  #format.json  { render :json => @questionnaires.to_json(:include => [ questionnaire_items: {include: :question}, questionnaire_items: {include: :content} ], ), :callback => params['callback']}

		end

	end

	def show
		@section = Section.find(params[:id])
		@questions = @section.questions
		respond_to do |format|
		  format.json  { render :json => @section.to_json(:include => [:questions]), :callback => params['callback']}
		end
	end

	def full
		render json: Section.all
	end

end