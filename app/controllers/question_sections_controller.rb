class QuestionSectionsController < ApplicationController

  before_filter :authorize
  
  before_action :set_question_section, only: [:show, :edit, :update, :destroy]

  # GET /question_sections
  # GET /question_sections.json
  def index
    @question_sections = QuestionSection.all
  end

  # GET /question_sections/1
  # GET /question_sections/1.json
  def show
  end

  # GET /question_sections/new
  def new
    @question_section = QuestionSection.new
  end

  # GET /question_sections/1/edit
  def edit
  end

  # POST /question_sections
  # POST /question_sections.json
  def create
    @question_section = QuestionSection.new(question_section_params)

    respond_to do |format|
      if @question_section.save
        format.html { redirect_to @question_section, notice: 'Question section was successfully created.' }
        format.json { render :show, status: :created, location: @question_section }
      else
        format.html { render :new }
        format.json { render json: @question_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /question_sections/1
  # PATCH/PUT /question_sections/1.json
  def update
    respond_to do |format|
      if @question_section.update(question_section_params)
        format.html { redirect_to @question_section, notice: 'Question section was successfully updated.' }
        format.json { render :show, status: :ok, location: @question_section }
      else
        format.html { render :edit }
        format.json { render json: @question_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /question_sections/1
  # DELETE /question_sections/1.json
  def destroy
    @question_section.destroy
    respond_to do |format|
      format.html { redirect_to question_sections_url, notice: 'Question section was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question_section
      @question_section = QuestionSection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_section_params
      params.require(:question_section).permit(:question_id, :section_id, :weight, :order)
    end
end
