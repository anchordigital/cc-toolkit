class ContentSerializer < ActiveModel::Serializer
  attributes :id, :heading, :text
end
