class QuestionSerializer < ActiveModel::Serializer
  attributes :id, :questions, :low_label, :high_label
end
