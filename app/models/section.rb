class Section < ActiveRecord::Base
	has_many :questions, :through => :question_sections
	has_many :question_inputs, :through => :questions
	has_many :question_sections
	has_many :feedbacks

  	accepts_nested_attributes_for :feedbacks, :reject_if => :all_blank, :allow_destroy => true
end
