class QuestionSection < ActiveRecord::Base
	belongs_to :section
	belongs_to :question

	accepts_nested_attributes_for :section, :reject_if => :all_blank, allow_destroy: true
	accepts_nested_attributes_for :question, :reject_if => :all_blank, allow_destroy: true
end
