class Questionnaire < ActiveRecord::Base
	has_many :questions, :through => :questionnaire_items
	has_many :contents, :through => :questionnaire_items
	has_many :questionnaire_items
end
