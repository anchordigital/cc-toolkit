class Question < ActiveRecord::Base
	has_many :sections, :through => :question_sections
	belongs_to :section
	belongs_to :questionnaire_items
	has_many :question_sections
	has_many :question_inputs
 	default_scope { order(id: :asc) }

 	accepts_nested_attributes_for :section, :reject_if => :all_blank, allow_destroy: true
 	accepts_nested_attributes_for :question_sections, :reject_if => :all_blank, allow_destroy: true
 	accepts_nested_attributes_for :question_inputs, :reject_if => :all_blank, allow_destroy: true
end
