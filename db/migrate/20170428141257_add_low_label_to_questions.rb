class AddLowLabelToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :low_label, :string
  end
end
