class CreateQuestionnaireItems < ActiveRecord::Migration
  def change
    create_table :questionnaire_items do |t|
      t.integer :question_id
      t.integer :content_id
      t.boolean :break
      t.integer :order

      t.timestamps null: false
    end
  end
end
