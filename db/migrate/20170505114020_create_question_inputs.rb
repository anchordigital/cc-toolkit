class CreateQuestionInputs < ActiveRecord::Migration
  def change
    create_table :question_inputs do |t|
      t.integer :question_id
      t.integer :order
      t.text :input

      t.timestamps null: false
    end
  end
end
