class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :heading
      t.string :text

      t.timestamps null: false
    end
  end
end
