class AutoIncOrder < ActiveRecord::Migration
	def change
    	change_column :questionnaire_items, :order, :int, null: false, unique: true, auto_increment: true
	end

end
