class UniqueKey < ActiveRecord::Migration
  def change
  	add_index :questionnaire_items, :order, unique: true
  end
end
