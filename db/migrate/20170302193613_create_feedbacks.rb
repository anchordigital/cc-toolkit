class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :name
      t.text :text
      t.integer :low
      t.integer :high
      t.integer :section_id

      t.timestamps null: false
    end
  end
end
