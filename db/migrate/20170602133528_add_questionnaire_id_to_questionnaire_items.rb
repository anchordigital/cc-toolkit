class AddQuestionnaireIdToQuestionnaireItems < ActiveRecord::Migration
  def change
    add_column :questionnaire_items, :questionnaire_id, :int
  end
end
