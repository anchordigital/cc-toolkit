class AddAuthorisedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :authorised, :boolean
  end
end
