# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170602133528) do

  create_table "contents", force: :cascade do |t|
    t.string   "heading",    limit: 255
    t.string   "text",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.text     "text",       limit: 65535
    t.integer  "low",        limit: 4
    t.integer  "high",       limit: 4
    t.integer  "section_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "question_inputs", force: :cascade do |t|
    t.integer  "question_id", limit: 4
    t.integer  "order",       limit: 4
    t.text     "input",       limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "question_sections", force: :cascade do |t|
    t.integer  "question_id", limit: 4
    t.integer  "section_id",  limit: 4
    t.integer  "weight",      limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "order",       limit: 4
  end

  create_table "questionnaire_items", force: :cascade do |t|
    t.integer  "question_id",      limit: 4
    t.integer  "content_id",       limit: 4
    t.boolean  "break"
    t.integer  "order",            limit: 4, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "questionnaire_id", limit: 4
  end

  add_index "questionnaire_items", ["order"], name: "index_questionnaire_items_on_order", unique: true, using: :btree

  create_table "questionnaires", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "questions", force: :cascade do |t|
    t.text     "question",   limit: 65535
    t.integer  "section_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "low_label",  limit: 255
    t.string   "high_label", limit: 255
  end

  create_table "sections", force: :cascade do |t|
    t.text     "name",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "authorised"
    t.boolean  "admin"
  end

end
