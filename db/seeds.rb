# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Question.create(question: "Question 1", section_id: 1)
Question.create(question: "Question 2", section_id: 1)
Question.create(question: "Question 3", section_id: 1)

Feedback.create(name: "First", text: "Lorem Ipsum", low: 1, high: 5, section_id: 1)
Feedback.create(name: "Second", text: "Lorem Ipsum", low: 6, high: 10, section_id: 1)

Section.create(name: "Section one")